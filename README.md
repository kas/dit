# DIT

Calculate Decimal Internet Time (DIT) according to the specification found on https://djeekay.net/dit/

### Requirements

* Python v3.6+ (uses formatted string literals)

### Installation

Copy `src/dit` to somewhere in your `$PATH`.

### Usage

Usage: `dit`

### Example

```sh

$ dit

1.35.79 DIT

```

### License

GNU General Public License v3+ ⌘ http://gplv3.fsf.org/

